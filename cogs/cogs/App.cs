﻿/*
 * Created by SharpDevelop.
 * User: wwlng1
 * Date: 9/12/2014
 * Time: 05:46 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Threading;
using System.Windows.Forms;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace cogs
{
	/// <summary>
	/// Description of Program.
	/// </summary>
	public class App
	{
		#region Main - Program entry point
		/// <summary>Program entry point.</summary>
		/// <param name="args">Command Line Arguments</param>
		[STAThread]
		public static void Main(string[] args)
		{
			
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
			
			bool isFirstInstance;
			// Please use a unique name for the mutex to prevent conflicts with other programs
			using (Mutex mtx = new Mutex(true, "cogs", out isFirstInstance)) {
				if (isFirstInstance) {
					
					var appContext = new AppContext();
                	Application.Run(appContext);
					
				} else {
					// The application is already running
					// TODO: Display message box or change focus to existing application instance
				}
			} // releases the Mutex
		}
		#endregion	
	}
}
