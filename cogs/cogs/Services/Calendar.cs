﻿/*
 * Created by SharpDevelop.
 * User: wwlng1
 * Date: 9/17/2014
 * Time: 12:01 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace cogs.Services
{
	/// <summary>
	/// Description of Calendar.
	/// </summary>
	public class Calendar
	{
		public string Id {get; set;}
		public string Name {get; set;}
		
		public Calendar(Google.Apis.Calendar.v3.Data.CalendarListEntry c)
		{
			Id = c.Id;
			Name = c.Summary;
		}
		
		public override string ToString()
		{
			return string.Format("[Calendar Id={0}, Name={1}]", Id, Name);
		}

		
	}
}
