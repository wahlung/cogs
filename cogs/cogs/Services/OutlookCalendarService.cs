﻿/*
 * Created by SharpDevelop.
 * User: wwlng1
 * Date: 9/15/2014
 * Time: 10:28 AM
 * 
 * 
 * 2015-02-17 Add ResetInstance for reseting Outlook instance after any failure 
 * 
 */
using System;
using System.Collections.Generic;
using Microsoft.Office.Interop.Outlook;


namespace cogs.Services
{
	/// <summary>
	/// Description of OutlookCalendar.
	/// </summary>
    public class OutlookCalendarService
    {
	    private static OutlookCalendarService instance;

        public static OutlookCalendarService Instance
        {
            get 
            {
                if (instance == null) instance = new OutlookCalendarService();
                return instance;
            }
        }
        
        
        public static void ResetInstance(){
        	// In case there is any issue in Outlook, it is better to forgive the old instance 
        	instance=null;
        }
        
        
        private readonly MAPIFolder CurrentOutlookCalendar;
        
        
        public OutlookCalendarService()
        {
        
            // Create the Outlook application.
            var oApp = new Application();

            // Get the NameSpace and Logon information.
            NameSpace oNS = oApp.GetNamespace("mapi");

            //Log on by using a dialog box to choose the profile.
            oNS.Logon("","", true, true);

            //Alternate logon method that uses a specific profile.
            // If you use this logon method, 
            // change the profile name to an appropriate value.
            //oNS.Logon("YourValidProfile", Missing.Value, false, true); 
			
            // Get the Calendar folder.
            CurrentOutlookCalendar = oNS.GetDefaultFolder(OlDefaultFolders.olFolderCalendar);

            // Done. Log off.
            oNS.Logoff();
        }
        
        
        public List<Appointment> getCalendarEntries()
        {
            Items OutlookItems = CurrentOutlookCalendar.Items;
            if (OutlookItems != null)
            {
                var result = new List<Appointment>();
                foreach (AppointmentItem ai in OutlookItems)
                {
                	result.Add(new Appointment(ai));
                }
                return result;
            }
            return null;
        }
        
        public List<Appointment> getCalendarEntriesInRange(int DaysInThePast, int DaysInTheFuture)
        {
            var result = new List<Appointment>();
            
            Items OutlookItems = CurrentOutlookCalendar.Items;
            OutlookItems.Sort("[Start]",Type.Missing);
            OutlookItems.IncludeRecurrences = true;
            
            if (OutlookItems != null)
            {
                DateTime min = DateTime.Now.AddDays(-DaysInThePast);
                DateTime max = DateTime.Now.AddDays(+DaysInTheFuture+1);
                string filter = "[End] >= '" + min.ToString("g") + "' AND [Start] < '" + max.ToString("g") + "'";
                
                foreach(AppointmentItem ai in OutlookItems.Restrict(filter))
                {
                	result.Add(new Appointment(ai));
                }
            }
            return result;
        }
        
        
        
    }

}
