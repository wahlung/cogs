﻿/*
 * Created by SharpDevelop.
 * User: wwlng1
 * Date: 9/17/2014
 * Time: 12:16 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Threading.Tasks;

using cogs.Services;

namespace cogs
{
	/// <summary>
	/// Interaction logic for SettingsWindow.xaml
	/// </summary>
	public partial class SettingsWindow : Window
	{
		
		public SettingsWindow()
		{
			
			InitializeComponent();

			txtSyncInterval.PreviewTextInput +=NumbericPreviewTextInput;
			txtDaysInTheFuture.PreviewTextInput +=NumbericPreviewTextInput;
			txtDaysInThePast.PreviewTextInput +=NumbericPreviewTextInput;

			this.Loaded += OnLoaded;
		}
		
		
		private async void OnLoaded(object sender, RoutedEventArgs e) {
			Mouse.OverrideCursor = Cursors.Wait;
			try {
				txtSyncInterval.Text = Settings.Default.SyncInterval.ToString();
				txtDaysInTheFuture.Text = Settings.Default.DaysInTheFuture.ToString();
				txtDaysInThePast.Text = Settings.Default.DaysInThePast.ToString();
				await GetCalendarListAsync();
			}
			finally{
				Mouse.OverrideCursor = null;
			}
		}
		
		private async Task GetCalendarListAsync(){
				GoogleCalendarService google = new GoogleCalendarService();
				cmbGoogleCalendarId.ItemsSource = await google.GetCalendarListAsync();
				cmbGoogleCalendarId.SelectedValue = Settings.Default.GoogleCalendarId;			
		}
		
		void btnOK_Click(object sender, RoutedEventArgs e)
		{
		
			if(cmbGoogleCalendarId.SelectedValue==null || cmbGoogleCalendarId.SelectedValue.Equals("")) {
				MessageBox.Show("You must select a Google Calendar.","Error", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation); 	
			}
			else {			
				Settings.Default.GoogleCalendarId=(string)cmbGoogleCalendarId.SelectedValue;
				Settings.Default.SyncInterval = int.Parse(txtSyncInterval.Text);
				Settings.Default.DaysInTheFuture = int.Parse(txtDaysInTheFuture.Text);
				Settings.Default.DaysInThePast = int.Parse(txtDaysInThePast.Text);

				Settings.Default.Save();
				this.Close();
			}
		}
		
		private void NumbericPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;

            }
        }
		
		
	}
}